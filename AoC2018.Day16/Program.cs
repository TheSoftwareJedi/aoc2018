﻿using AoC2018.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AoC2018.Day16
{
    internal class Program
    {
        private static void Main(string[] args) =>
            //Part1();
            Part2();

        private static void Part1()
        {
            string[] lines = File.ReadAllLines("input.txt");

            Action<int[], int[]> addr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] + reg[op[2]];
            Action<int[], int[]> addi = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] + op[2];

            Action<int[], int[]> mulr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] * reg[op[2]];
            Action<int[], int[]> muli = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] * op[2];

            Action<int[], int[]> banr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] & reg[op[2]];
            Action<int[], int[]> bani = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] & op[2];

            Action<int[], int[]> borr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] | reg[op[2]];
            Action<int[], int[]> bori = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] | op[2];

            Action<int[], int[]> setr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]];
            Action<int[], int[]> seti = (int[] reg, int[] op) => reg[op[3]] = op[1];

            Action<int[], int[]> gtir = (int[] reg, int[] op) => reg[op[3]] = op[1] > reg[op[2]] ? 1 : 0;
            Action<int[], int[]> gtri = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] > op[2] ? 1 : 0;
            Action<int[], int[]> gtrr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] > reg[op[2]] ? 1 : 0;

            Action<int[], int[]> eqir = (int[] reg, int[] op) => reg[op[3]] = op[1] == reg[op[2]] ? 1 : 0;
            Action<int[], int[]> eqri = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] == op[2] ? 1 : 0;
            Action<int[], int[]> eqrr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] == reg[op[2]] ? 1 : 0;

            Action<int[], int[]>[] ops = new[] { addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr };

            /*
Before: [0, 1, 2, 1]
14 1 3 3
After:  [0, 1, 2, 1]
*/
            int count = 0;
            char[] chars = new char[] { '[', ',', ' ', ']' };
            for (int i = 0; i < lines.Length; i++)
            {
                int[] reg = lines[i++].Split(chars, StringSplitOptions.RemoveEmptyEntries).Skip(1).Select(a => a.To<int>()).ToArray();
                int[] curOp = lines[i++].Split(chars, StringSplitOptions.RemoveEmptyEntries).Select(a => a.To<int>()).ToArray();
                int[] regOut = lines[i++].Split(chars, StringSplitOptions.RemoveEmptyEntries).Skip(1).Select(a => a.To<int>()).ToArray();

                int inCount = 0;
                foreach (Action<int[], int[]> op in ops)
                {
                    int[] r = (int[])reg.Clone();
                    op(r, curOp);
                    if (r[0] == regOut[0] && r[1] == regOut[1] && r[2] == regOut[2] && r[3] == regOut[3])
                    {
                        inCount++;
                        if (inCount == 3)
                        {
                            count++;
                            break;
                        }
                    }
                }
            }

            Console.WriteLine(count);
            Console.ReadLine();
        }

        private static void Part2()
        {
            Dictionary<int, Action<int[], int[]>> opLookup = GetOpDictionary();

            var lines = File.ReadAllLines("input2.txt");
            var reg = new int[] { 0, 0, 0, 0 };
            foreach (var line in lines)
            {
                var op = line.Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(a => a.To<int>()).ToArray();
                opLookup[op[0]](reg, op);
            }

            Console.WriteLine(reg[0]);
            Console.ReadLine();
        }

        private static Dictionary<int, Action<int[], int[]>> GetOpDictionary()
        {
            string[] lines = File.ReadAllLines("input.txt");

            Action<int[], int[]> addr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] + reg[op[2]];
            Action<int[], int[]> addi = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] + op[2];

            Action<int[], int[]> mulr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] * reg[op[2]];
            Action<int[], int[]> muli = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] * op[2];

            Action<int[], int[]> banr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] & reg[op[2]];
            Action<int[], int[]> bani = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] & op[2];

            Action<int[], int[]> borr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] | reg[op[2]];
            Action<int[], int[]> bori = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] | op[2];

            Action<int[], int[]> setr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]];
            Action<int[], int[]> seti = (int[] reg, int[] op) => reg[op[3]] = op[1];

            Action<int[], int[]> gtir = (int[] reg, int[] op) => reg[op[3]] = op[1] > reg[op[2]] ? 1 : 0;
            Action<int[], int[]> gtri = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] > op[2] ? 1 : 0;
            Action<int[], int[]> gtrr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] > reg[op[2]] ? 1 : 0;

            Action<int[], int[]> eqir = (int[] reg, int[] op) => reg[op[3]] = op[1] == reg[op[2]] ? 1 : 0;
            Action<int[], int[]> eqri = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] == op[2] ? 1 : 0;
            Action<int[], int[]> eqrr = (int[] reg, int[] op) => reg[op[3]] = reg[op[1]] == reg[op[2]] ? 1 : 0;

            Action<int[], int[]>[] ops = new[] { addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr };

            /*
Before: [0, 1, 2, 1]
14 1 3 3
After:  [0, 1, 2, 1]
*/
            char[] chars = new char[] { '[', ',', ' ', ']' };
            var outcomes = new List<Outcome>();
            for (int i = 0; i < lines.Length; i++)
            {
                int[] reg = lines[i++].Split(chars, StringSplitOptions.RemoveEmptyEntries).Skip(1).Select(a => a.To<int>()).ToArray();
                int[] curOp = lines[i++].Split(chars, StringSplitOptions.RemoveEmptyEntries).Select(a => a.To<int>()).ToArray();
                int[] regOut = lines[i++].Split(chars, StringSplitOptions.RemoveEmptyEntries).Skip(1).Select(a => a.To<int>()).ToArray();
                outcomes.Add(new Outcome()
                {
                    opNum = curOp[0],
                    registers = reg,
                    operation = curOp,
                    result = regOut
                });
            }

            var opLookup = new Dictionary<int, Action<int[], int[]>>();
            var opLookupPossibles = new Dictionary<int, HashSet<Action<int[], int[]>>>();

            for (int opIndex = 0; opIndex < 16; opIndex++)
            {
                IEnumerable<Outcome> tests = outcomes.Where(a => a.opNum == opIndex);
                var matches = new HashSet<Action<int[], int[]>>();
                foreach (Action<int[], int[]> op in ops)
                {
                    if (!opLookup.Values.Contains(op) &&
                        tests.All(test =>
                        {
                            int[] r = (int[])test.registers.Clone();
                            op(r, test.operation);
                            if (r[0] != test.result[0] || r[1] != test.result[1] || r[2] != test.result[2] || r[3] != test.result[3])
                                return false;
                            return true;
                        }))
                    {
                        matches.Add(op);
                    }
                }
                if (matches.Count == 1)
                    opLookup[opIndex] = matches.First();
                else
                {
                    opLookupPossibles[opIndex] = new HashSet<Action<int[], int[]>>();
                    matches.ForEachImmediate(a => opLookupPossibles[opIndex].Add(a));
                }
            }

            while (opLookupPossibles.Count > 0)
            {
                foreach (KeyValuePair<int, HashSet<Action<int[], int[]>>> item in opLookupPossibles)
                    item.Value.RemoveWhere(a => opLookup.Values.Contains(a));
                var found = opLookupPossibles.Where(a => a.Value.Count == 1).First();
                opLookupPossibles.Remove(found.Key);
                opLookup[found.Key] = found.Value.First();
            }

            return opLookup;
        }
    }

    public class Outcome
    {
        public int opNum;
        public int[] registers;
        public int[] operation;
        public int[] result;
    }
}
