﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using AoC2018.Common;

namespace AoC2018.Day13
{
    class Program
    {
        static void Main(string[] args)
        {
            //Part1();
            //Console.ReadLine();
            Part2();
            Console.ReadLine();
        }

        private static void Part1()
        {
            var lines = File.ReadAllLines("input.txt");
            //track without carts
            var track = lines.Select(a => a.Select(b => b == '>' || b == '<' ? '-' : b == '^' || b == 'v' ? '|' : b).ToArray()).ToArray();
            var cartChars = new [] { '<', '>', 'v', '^'};
            var carts = lines.Select((a, i) => (line: a, y: i)).SelectMany(a => a.line.Select((b, i) => new Cart() { c = b, x = i, y = a.y, turnState = 0 })).Where(a => cartChars.Contains(a.c)).ToArray();

            while (true)
            {
                var cartArray = carts.OrderBy(a => a.y).ThenBy(a => a.x).ToArray();
                for (int i = 0; i < cartArray.Length; i++)
                {
                    var cart = cartArray[i];
                    if (cart.c == '>')
                    {
                        cart.x++;
                        if (track[cart.y][cart.x] == '\\')
                            cart.c = 'v';
                        else if (track[cart.y][cart.x] == '/')
                            cart.c = '^';
                        else if (track[cart.y][cart.x] == '+')
                        {
                            cart.c = cart.turnState == 0 ? '^' : cart.turnState == 2 ? 'v' : cart.c;
                            cart.turnState++;
                            if (cart.turnState == 3)
                                cart.turnState = 0;
                        }
                    }
                    else if (cart.c == '<')
                    {
                        cart.x--;
                        if (track[cart.y][cart.x] == '\\')
                            cart.c = '^';
                        else if (track[cart.y][cart.x] == '/')
                            cart.c = 'v';
                        else if (track[cart.y][cart.x] == '+')
                        {
                            cart.c = cart.turnState == 0 ? 'v' : cart.turnState == 2 ? '^' : cart.c;
                            cart.turnState++;
                            if (cart.turnState == 3)
                                cart.turnState = 0;
                        }
                    }
                    else if (cart.c == '^')
                    {
                        cart.y--;
                        if (track[cart.y][cart.x] == '\\')
                            cart.c = '<';
                        else if (track[cart.y][cart.x] == '/')
                            cart.c = '>';
                        else if (track[cart.y][cart.x] == '+')
                        {
                            cart.c = cart.turnState == 0 ? '<' : cart.turnState == 2 ? '>' : cart.c;
                            cart.turnState++;
                            if (cart.turnState == 3)
                                cart.turnState = 0;
                        }
                    }
                    else if (cart.c == 'v')
                    {
                        cart.y++;
                        if (track[cart.y][cart.x] == '\\')
                            cart.c = '>';
                        else if (track[cart.y][cart.x] == '/')
                            cart.c = '<';
                        else if (track[cart.y][cart.x] == '+')
                        {
                            cart.c = cart.turnState == 0 ? '>' : cart.turnState == 2 ? '<' : cart.c;
                            cart.turnState++;
                            if (cart.turnState == 3)
                                cart.turnState = 0;
                        }
                    }
                    //check crash
                    var crashes = carts.Where(a => carts.Count(b => a.x == b.x && a.y == b.y) > 1).ToArray();
                    if (crashes.Any())
                    {
                        var crash = crashes.First();
                        Console.WriteLine($@"crash at {crash.x}, {crash.y}");
                        return;
                    }
                }
            }
        }

        private static void Part2()
        {
            var lines = File.ReadAllLines("input.txt");
            //track without carts
            var track = lines.Select(a => a.Select(b => b == '>' || b == '<' ? '-' : b == '^' || b == 'v' ? '|' : b).ToArray()).ToArray();
            var cartChars = new[] { '<', '>', 'v', '^' };
            var carts = lines.Select((a, i) => (line: a, y: i)).SelectMany(a => a.line.Select((b, i) => new Cart() { c = b, x = i, y = a.y, turnState = 0 })).Where(a => cartChars.Contains(a.c)).ToList();

            while (true)
            {
                var cartArray = carts.OrderBy(a => a.y).ThenBy(a => a.x).ToArray();
                for (int i = 0; i < cartArray.Length; i++)
                {
                    var cart = cartArray[i];

                    if (cart.crashed)
                        continue;

                    if (cart.c == '>')
                    {
                        cart.x++;
                        if (track[cart.y][cart.x] == '\\')
                            cart.c = 'v';
                        else if (track[cart.y][cart.x] == '/')
                            cart.c = '^';
                        else if (track[cart.y][cart.x] == '+')
                        {
                            cart.c = cart.turnState == 0 ? '^' : cart.turnState == 2 ? 'v' : cart.c;
                            cart.turnState++;
                            if (cart.turnState == 3)
                                cart.turnState = 0;
                        }
                    }
                    else if (cart.c == '<')
                    {
                        cart.x--;
                        if (track[cart.y][cart.x] == '\\')
                            cart.c = '^';
                        else if (track[cart.y][cart.x] == '/')
                            cart.c = 'v';
                        else if (track[cart.y][cart.x] == '+')
                        {
                            cart.c = cart.turnState == 0 ? 'v' : cart.turnState == 2 ? '^' : cart.c;
                            cart.turnState++;
                            if (cart.turnState == 3)
                                cart.turnState = 0;
                        }
                    }
                    else if (cart.c == '^')
                    {
                        cart.y--;
                        if (track[cart.y][cart.x] == '\\')
                            cart.c = '<';
                        else if (track[cart.y][cart.x] == '/')
                            cart.c = '>';
                        else if (track[cart.y][cart.x] == '+')
                        {
                            cart.c = cart.turnState == 0 ? '<' : cart.turnState == 2 ? '>' : cart.c;
                            cart.turnState++;
                            if (cart.turnState == 3)
                                cart.turnState = 0;
                        }
                    }
                    else if (cart.c == 'v')
                    {
                        cart.y++;
                        if (track[cart.y][cart.x] == '\\')
                            cart.c = '>';
                        else if (track[cart.y][cart.x] == '/')
                            cart.c = '<';
                        else if (track[cart.y][cart.x] == '+')
                        {
                            cart.c = cart.turnState == 0 ? '>' : cart.turnState == 2 ? '<' : cart.c;
                            cart.turnState++;
                            if (cart.turnState == 3)
                                cart.turnState = 0;
                        }
                    }
                    //check crash
                    var crashes = carts.Where(a => carts.Count(b => a.x == b.x && a.y == b.y) > 1).ToArray();
                    if (crashes.Any())
                    {
                        foreach (var crash in crashes)
                        {
                            carts.Remove(crash);
                            crash.crashed = true;
                        }
                    }
                }
                if (carts.Count() == 1)
                {
                    Console.WriteLine($@"{carts.First().x},{carts.First().y}");
                    return;
                }
            }
        }
    }

    public class Cart
    {
        public int x;
        public int y;
        public int turnState;
        public char c;
        public bool crashed = false;
    }
}
