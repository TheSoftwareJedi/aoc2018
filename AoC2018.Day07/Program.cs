﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AoC2018.Common;

namespace AoC2018.Day7
{
    class Program
    {

        static void Main(string[] args)
        {
            //Part1();
            Part2();
        }

        private static void Part1()
        {
            var r = new Regex("Step (.{1}).*ep (.{1}).*");
            var tuples = File.ReadAllLines("input.txt").Select(a => r.Match(a)).Select(a => (a.Groups[1].Value.To<char>(), a.Groups[2].Value.To<char>()));
            var nodeDictionary = new Dictionary<char, Node>();
            foreach (var t in tuples)
            {
                var n1 = nodeDictionary.GetValueOrDefaultWithAdd(t.Item1, new Node() { Value = t.Item1 });
                var n2 = nodeDictionary.GetValueOrDefaultWithAdd(t.Item2, new Node() { Value = t.Item2 });
                n1.Children.Add(n2);
                n2.Parents.Add(n1);
            }
            var firsts = nodeDictionary.Where(a => !a.Value.Parents.Any());

            var finalOrder = new List<char>();
            var eligbleForVisit = new List<Node>();
            eligbleForVisit.AddRange(firsts.Select(a => a.Value));
            while (eligbleForVisit.Count > 0)
            {
                var node = eligbleForVisit.Where(a => !a.Parents.Any() || a.Parents.All(b => b.Visited)).OrderBy(a => a.Value).First();
                eligbleForVisit.RemoveAll(a => a.Value == node.Value);
                eligbleForVisit.AddRange(node.Children);
                node.Visited = true;
                finalOrder.Add(node.Value);
            }

            Console.WriteLine(new string(finalOrder.ToArray()));
            Console.ReadLine();
        }

        private static void Part2()
        {
            var r = new Regex("Step (.{1}).*ep (.{1}).*");
            var tuples = File.ReadAllLines("input.txt").Select(a => r.Match(a)).Select(a => (a.Groups[1].Value.To<char>(), a.Groups[2].Value.To<char>()));
            var nodeDictionary = new Dictionary<char, Node>();
            foreach (var t in tuples)
            {
                var n1 = nodeDictionary.GetValueOrDefaultWithAdd(t.Item1, new Node() { Value = t.Item1 });
                var n2 = nodeDictionary.GetValueOrDefaultWithAdd(t.Item2, new Node() { Value = t.Item2 });
                n1.Children.Add(n2);
                n2.Parents.Add(n1);
            }
            var firsts = nodeDictionary.Where(a => !a.Value.Parents.Any());

            var currentString = "";
            var eligbleForVisit = new List<Node>();
            eligbleForVisit.AddRange(firsts.Select(a => a.Value));
            int second = -1;
            int availableElves = 5;
            while (nodeDictionary.Where(a => !a.Value.Visited).Count() > 0)
            {
                second++;
                var visitingNodes = nodeDictionary.Where(a => a.Value.Visiting).Select(a => a.Value);
                foreach (var visitingNode in visitingNodes)
                {
                    visitingNode.TimeRemaining--;
                    if (visitingNode.TimeRemaining == 0)
                    {
                        visitingNode.Visiting = false;
                        visitingNode.Visited = true;
                        availableElves++;
                        currentString += visitingNode.Value;
                    }
                }

                while (availableElves > 0 && eligbleForVisit.Where(a => !a.Visiting && (!a.Parents.Any() || a.Parents.All(b => b.Visited))).Count() > 0)
                {
                    var node = eligbleForVisit.Where(a => !a.Visiting && (!a.Parents.Any() || a.Parents.All(b => b.Visited))).OrderBy(a => a.Value).FirstOrDefault();
                    if (node != null)
                    {
                        availableElves--;
                        eligbleForVisit.RemoveAll(a => a.Value == node.Value);
                        eligbleForVisit.AddRange(node.Children);
                        node.Visiting = true;
                    }
                }
                Console.WriteLine($"{second}, ${availableElves}, {currentString}, {new string(eligbleForVisit.Select(a=>a.Value).ToArray())}");
            }

            Console.WriteLine(second);
            Console.ReadLine();
        }

        public class Node
        {
            public List<Node> Parents = new List<Node>();
            public List<Node> Children = new List<Node>();
            private char _value;
            public char Value { get => _value; set { this._value = value; TimeRemaining = value - 64 + 60; } }
            public bool Visiting = false;
            public bool Visited = false;
            public int TimeRemaining;

        }
    }
}
