﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AoC2018.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AoC2018.Day1
{
    class Program
    {
        static void Main(string[] args)
        {
            TestExamples();
            Part1();
            Part2();
}

        private static void Part1()
        {
            var i = File.ReadAllLines($"input.txt").Where(a => a?.Trim() != "").Select(a => a.To<int>()).Sum();
            Console.WriteLine(i);
            Console.ReadLine();
        }

        private static void Part2()
        {
            var endlessFreqs = File.ReadAllLines($"input.txt").Where(a => a?.Trim() != "").Select(a => a.To<int>()).Endless();
            var i = FirstDupeFreq(endlessFreqs);
            Console.WriteLine(i);
            Console.ReadLine();
        }

        private static int FirstDupeFreq(IEnumerable<int> endlessFreqs)
        {
            var record = new HashSet<int>();
            var currentFreq = 0;
            foreach (var freq in endlessFreqs)
            {
                record.Add(currentFreq);
                currentFreq += freq;
                if (record.Contains(currentFreq))
                    break;
                record.Add(currentFreq);
            }
            return currentFreq;
        }

        private static void TestExamples()
        {
            int i;
            i = FirstDupeFreq((new[] { 3, 3, 4, -2, -4 }).Endless());
            Assert.AreEqual(10, i);
            i = FirstDupeFreq((new[] { +1, -1 }).Endless());
            Assert.AreEqual(0, i);
            i = FirstDupeFreq((new[] { -6, +3, +8, +5, -6 }).Endless());
            Assert.AreEqual(5, i);
            i = FirstDupeFreq((new[] { +7, +7, -2, -7, -4 }).Endless());
            Assert.AreEqual(14, i);
        }

    }
}
