﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using AoC2018.Common;

namespace AoC2018.Day8
{
    class Program
    {
        static void Main(string[] args)
        {
            //Part1();
            Part2();
        }

        private static void Part1()
        {
            var numbers = File.ReadAllLines("input.txt").First().Split(' ').Select(a => a.To<int>()).ToList();
            int total = Handle(numbers);

            Console.WriteLine(total);
            Console.ReadLine();
        }

        private static int Handle(List<int> numbers)
        {
            var total = 0;
            var header = numbers.Take(2).ToArray();
            numbers.RemoveRange(0, 2);
            for (int i = 0; i < header[0]; i++)
                total += Handle(numbers);
            total += numbers.Take(header[1]).Sum();
            numbers.RemoveRange(0, header[1]);
            return total;
        }




        private static void Part2()
        {
            var numbers = File.ReadAllLines("input.txt").First().Split(' ').Select(a => a.To<int>()).ToList();


            var node = new Node();
            Handle2(numbers, node);
            int answer = Sum(node);

            Console.WriteLine(answer);
            Console.ReadLine();
        }

        private static void Handle2(List<int> numbers, Node node)
        {
            var header = numbers.Take(2).ToArray();
            numbers.RemoveRange(0, 2);
            for (int i = 0; i < header[0]; i++)
            {
                var newNode = new Node();
                Handle2(numbers, newNode);
                node.ChildNodes.Add(newNode);
            }

            node.ChildIndexes = numbers.Take(header[1]).ToList();
            node.MetaData = numbers.Take(header[1]).ToList();
            numbers.RemoveRange(0, header[1]);
        }

        private static int Sum(Node node)
        {
            if (node == null)
                return 0;
            var total = 0;
            foreach (var index in node.ChildIndexes)
                total += Sum(node?.ChildNodes?.ElementAtOrDefault(index-1));
            if (!node.ChildNodes.Any())
                total += node.Value;
            return total;
        }

        public class Node
        {
            public int Value {  get { return MetaData.Sum(); } }
            public List<int> ChildIndexes;
            public List<int> MetaData;
            public List<Node> ChildNodes = new List<Node>();
        }
    }
}
