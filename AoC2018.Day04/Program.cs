﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using AoC2018.Common;
using System.Text.RegularExpressions;

namespace AoC2018.AtWorkTemp
{
    class Program
    {
        static void Main(string[] args)
        {
            var lines = File.ReadAllLines("input.txt");
            Test1(lines);
        }

        private static void Test1(string[] lines)
        {
            var r = new Regex(@"\[((\d\d\d\d-\d\d-\d\d) (\d\d):(\d\d))] (.*)");
            var logData = lines.Select(a => a.Replace("[15", "[20")).Where(a => a.Trim() != "")
                .Select(a => r.Match(a))
                .Select(a => (Date: DateTime.ParseExact(a.Groups[1].Value, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                                    Day: a.Groups[2].Value,
                                    Hour: a.Groups[3].Value.To<int>(),
                                    Minute: a.Groups[4].Value.To<int>(),
                                    Detail: a.Groups[5].Value))
                 .OrderBy(a => a.Date)
                 .ToArray();

            var d = new Dictionary<int, int[]>();
            var currentGuard = -1;
            bool isSleeping = false;
            r = new Regex(@".*#(\d+) .*");
            var item = 0;

            foreach (var min in GetMinsBetween(logData.Min(a => a.Date), logData.Max(a => a.Date)))
            {
                if (item > logData.Length - 1) { } //EOF
                else if (item > 0 && min < logData[item].Date) { } //not ready for this line
                else if (logData[item].Detail.Contains("#"))
                {
                    isSleeping = false;
                    currentGuard = r.Match(logData[item].Detail).Groups[1].Value.To<int>();
                    if (!d.ContainsKey(currentGuard))
                        d[currentGuard] = new int[60];
                    item++;
                }
                else if (logData[item].Detail.StartsWith('f'))
                {
                    isSleeping = true;
                    item++;
                }
                else if (logData[item].Detail.StartsWith('w'))
                {
                    isSleeping = false;
                    item++;
                }

                if (currentGuard != -1 && isSleeping && (item > logData.Length - 1 ||  logData[item - 1].Hour == 0))
                    d[currentGuard][min.Minute]++;
            }

            {
                var sleepyGuards = d.Select(a => (a.Key, a.Value.Sum())).OrderByDescending(a => a.Item2).ToArray();
                var sleepyGuard = sleepyGuards.First();
                Console.WriteLine(sleepyGuard.Key);
                var sleepyHour = 0;
                var sleepyMax = 0;
                for (int i = 0; i < 60; i++)
                    if (d[sleepyGuard.Key][i] > sleepyMax)
                    {
                        sleepyMax = d[sleepyGuard.Key][i];
                        sleepyHour = i;
                    }
                Console.WriteLine(sleepyHour);
                Console.ReadLine();
            }

            {
                Console.WriteLine("Part 2");
                var sleepyGuards = d.Select(a => (a.Key, a.Value.Max())).OrderByDescending(a => a.Item2).ToArray();
                var sleepyGuard = sleepyGuards.First();
                Console.WriteLine(sleepyGuard.Key);
                var sleepyHour = 0;
                var sleepyMax = 0;
                for (int i = 0; i < 60; i++)
                    if (d[sleepyGuard.Key][i] > sleepyMax)
                    {
                        sleepyMax = d[sleepyGuard.Key][i];
                        sleepyHour = i;
                    }
                Console.WriteLine(sleepyHour);
                Console.ReadLine();
            }
        }

        private static IEnumerable<DateTime> GetMinsBetween(DateTime minDate, DateTime maxDate)
        {
            minDate = minDate.Date;
            maxDate = maxDate.Date.AddHours(1);
            while (minDate <= maxDate)
            {
                for (int i = 0; i < 59; i++)
                {
                    yield return minDate;
                    minDate = minDate.AddMinutes(1);
                }
                minDate = minDate.AddDays(1).Date;
            }
        }
    }
}
