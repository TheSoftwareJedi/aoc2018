﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AoC2018.Day2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Part1();
            Part2();
        }

        private static void Part1()
        {
            var twos = 0;
            var threes = 0;
            var lines = File.ReadAllLines($"input.txt").Where(a => a?.Trim() != "");
            foreach (var line in lines)
            {
                if (HasNum(line, 2))
                    twos++;
                if (HasNum(line, 3))
                    threes++;
            }
            Console.WriteLine(twos);
            Console.WriteLine(threes);
            Console.WriteLine(twos * threes);
            Console.ReadLine();
        }

        private static bool HasNum(string line, int number)
        {
            var letters = line.Distinct();
            foreach (var letter in letters)
                if (line.Count(a => a == letter) == number)
                    return true;
            return false;
        }

        private static void Part2()
        {
            var lines = File.ReadAllLines($"input.txt").Where(a => a?.Trim() != "");
            foreach (var line1 in lines)
                foreach (var line2 in lines.Where(a => a != line1))
                {
                    char[] cArray1 = line1.ToCharArray();
                    char[] cArray2 = line2.ToCharArray();
                    var pos = CheckArrays(cArray1, cArray2);
                    if (pos >= 0)
                    {
                        var badChar = cArray1[pos];
                        var badArray = cArray1.Where(a => a != badChar).ToArray();
                        var sAnswer = new string(badArray);
                        Console.WriteLine(badArray);
                        Console.ReadLine();
                        return;
                    }
                }
        }

        private static int CheckArrays(char[] cArray1, char[] cArray2)
        {
            var finalPos = -1;
            for (int i = 0; i < cArray1.Length; i++)
            {
                if (cArray1[i] != cArray2[i])
                    if (finalPos >= 0)
                        return -1;
                    else
                        finalPos = i;
            }
            return finalPos;
        }
    }
}
