﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using AoC2018.Common;

namespace AoC2018.Day9
{
    class Program
    {
        static void Main(string[] args)
        {
            //var answer = Part1();
            //Console.WriteLine(answer);
            //Console.ReadLine();

            var answer = Part2();
            Console.WriteLine("final: " + answer);
            Console.ReadLine();
        }

        private static int Part1()
        {
            var lines = File.ReadAllLines("input.txt");
            var regexed = Regex.Match(lines.First(), @"(\d+) players; last marble is worth (\d+) points");
            var (players, lastMarble) = (regexed.Groups[1].Value.To<int>(), regexed.Groups[2].Value.To<int>());

            var scores = new Dictionary<int, int>();
            var marbles = new LinkedList<int>();
            for (int i = 0; i < players; i++)
                scores.Add(i, 0);
            var m = 0;
            var c = marbles.AddLast(m++);
            while (true)
            {
                for (int p = 0; p < players; p++)
                {
                    if (m % 23 == 0)
                    {
                        var roundScore = 0;
                        roundScore += m;
                        for (int i = 0; i < 7; i++)
                        {
                            c = c.Previous ?? c.List.Last;
                        }
                        roundScore += c.Value;

                        var newC = c.Next ?? marbles.First;
                        marbles.Remove(c);
                        c = newC;

                        scores[p] += roundScore;
                        Console.WriteLine(m);
                    }
                    else
                    {
                        c = c.Next ?? marbles.First;
                        marbles.AddAfter(c, m);
                        c = c.Next;
                    }
                    if (m == lastMarble)
                    {
                        return scores.Max(a => a.Value);
                    }
                    m++;
                }
            }
        }

        private static long Part2()
        {
            var lines = File.ReadAllLines("input.txt");
            var regexed = Regex.Match(lines.First(), @"(\d+) players; last marble is worth (\d+) points");
            var (players, lastMarble) = (regexed.Groups[1].Value.To<int>(), regexed.Groups[2].Value.To<int>() * 100);

            var scores = new Dictionary<int, long>();
            var marbles = new LinkedList<int>();
            for (int i = 0; i < players; i++)
                scores.Add(i, 0);
            var m = 0;
            var c = marbles.AddLast(m++);
            while (true)
            {
                for (int p = 0; p < players; p++)
                {
                    if (m % 23 == 0)
                    {
                        var roundScore = 0;
                        roundScore += m;
                        for (int i = 0; i < 7; i++)
                        {
                            c = c.Previous ?? c.List.Last;
                        }
                        roundScore += c.Value;

                        var newC = c.Next ?? marbles.First;
                        marbles.Remove(c);
                        c = newC;

                        scores[p] += roundScore;
                        Console.WriteLine(m);
                    }
                    else
                    {
                        c = c.Next ?? marbles.First;
                        marbles.AddAfter(c, m);
                        c = c.Next;
                    }
                    if (m == lastMarble)
                    {
                        return scores.Max(a => a.Value);
                    }
                    m++;
                }
            }
        }
    }
}
