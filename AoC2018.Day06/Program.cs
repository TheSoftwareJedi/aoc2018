﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AoC2018.Common;

namespace AoC2018.Day6
{
    class Program
    {
        static void Main(string[] args)
        {
            Part1();
            Part2();
        }

        private static void Part1()
        {
            var points = File.ReadAllLines("input.txt").Select(a => a.Split(',', ' ', StringSplitOptions.RemoveEmptyEntries)).Select((a, b) => (idx: b, x: a[0].To<int>(), y: a[1].To<int>())).ToArray();
            var minX = points.Min(a => a.x);
            var maxX = points.Max(a => a.x);
            var minY = points.Min(a => a.y);
            var maxY = points.Max(a => a.y);
            int[][] graph = new int[maxX + 1][];
            for (int i = 0; i < graph.Length; i++)
                graph[i] = new int[maxY + 1];

            for (int i = 0; i < graph.Length; i++)
                for (int j = 0; j < graph[i].Length; j++)
                {
                    (int idx, int min) min = (-1, int.MaxValue);
                    for (int p = 0; p < points.Length; p++)
                        if ((points[p].x, points[p].y) != (i, j))
                        {
                            var dist = CalculateManhattanDistance(points[p].x, points[p].y, i, j);
                            if (dist < min.min) {
                                min.min = dist;
                                min.idx = points[p].idx;
                            } else if (dist == min.min)
                                min.idx = -1;
                        } else
                        {
                            min.min = 0;
                            min.idx = points[p].idx;
                        }
                    graph[i][j] = min.idx;
                }
            //exclude edges
            var no = graph[0].Distinct().Concat(graph[maxX].Distinct()).Concat(graph.Select(a => a[0]).Distinct()).Concat(graph.Select(a => a[maxY - 1])).Distinct().ToHashSet();
            no.Add(-1);

            var dict = new Dictionary<int, int>();
            foreach (var p in points)
                dict.Add(p.idx, 0);
            dict.Add(-1, 0);

            for (int i = 0; i < graph.Length; i++)
                for (int j = 0; j < graph[i].Length; j++)
                    dict[graph[i][j]]++;

            var answer = dict.Where(a => !no.Contains(a.Key)).Max(a => a.Value);
            Console.WriteLine(answer);
            Console.ReadLine();
        }

        private static void Part2()
        {
            var points = File.ReadAllLines("input.txt").Select(a => a.Split(',', ' ', StringSplitOptions.RemoveEmptyEntries)).Select((a, b) => (idx: b, x: a[0].To<int>(), y: a[1].To<int>())).ToArray();
            var minX = points.Min(a => a.x);
            var maxX = points.Max(a => a.x);
            var minY = points.Min(a => a.y);
            var maxY = points.Max(a => a.y);
            int[][] graph = new int[maxX + 1][];
            for (int i = 0; i < graph.Length; i++)
                graph[i] = new int[maxY + 1];

            for (int i = 0; i < graph.Length; i++)
                for (int j = 0; j < graph[i].Length; j++)
                    for (int p = 0; p < points.Length; p++)
                        graph[i][j] = points.Select(a => CalculateManhattanDistance(i, j, a.x, a.y)).Sum() < 10000 ? int.MinValue : int.MaxValue;

            var answer = graph.SelectMany(a => a).Where(a => a == int.MinValue).Count();
            Console.WriteLine(answer);
            Console.ReadLine();
        }

        public static int CalculateManhattanDistance(int x1, int y1, int x2, int y2) => Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
    }
}

//3,4 5,5 4,5
