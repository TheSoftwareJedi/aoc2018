﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AoC2018.Common;

namespace AoC2018.Day3
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestExamples();
            //Part1();
            Part2();
        }

        private static void Part1()
        {
            Row[] rows = GetRows();
            int[][] fabric = MakeFabric(rows);

            var overlapped = fabric.SelectMany(a => a).Count(a => a > 1);

            Console.WriteLine(overlapped);
        }

        private static void Part2()
        {
            var rows = GetRows();
            var fabric = MakeFabric(rows);

            foreach (var row in rows)
                if (CheckForGoodElf(row, fabric))
                    Console.WriteLine(row.Elf);
        }

        private static bool CheckForGoodElf(Row row, int[][] fabric)
        {
            for (int i = row.Left; i < row.Left + row.Width; i++)
                for (int j = row.Top; j < row.Top + row.Height; j++)
                    if (fabric[i][j] > 1)
                        return false;
            return true;
        }

        private static Row[] GetRows()
        {
            var r = new Regex(@"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)");
            var rows = File.ReadAllLines($"input.txt").Where(a => a?.Trim() != "").Select(a => r.Match(a)).Select(a =>
              new Row()
              {
                  Elf = a.Groups[1].ToString().To<int>(),
                  Left = a.Groups[2].ToString().To<int>(),
                  Top = a.Groups[3].ToString().To<int>(),
                  Width = a.Groups[4].ToString().To<int>(),
                  Height = a.Groups[5].ToString().To<int>()
              }).ToArray();
            return rows;
        }

        private static int[][] MakeFabric(Row[] rows)
        {
            var maxW = rows.Max(a => a.Left + a.Width);
            var maxH = rows.Max(a => a.Top + a.Height);
            var fabric = new int[maxW][];
            for (int i = 0; i < fabric.Length; i++)
                fabric[i] = new int[maxH];

            foreach (var row in rows)
                for (int i = row.Left; i < row.Left + row.Width; i++)
                    for (int j = row.Top; j < row.Top + row.Height; j++)
                        fabric[i][j]++;
            return fabric;
        }

        private static void ProcessRow(Row row, int[,] fabric)
        {
        }
    }

    public class Row
    {
        public int Elf;
        public int Left;
        public int Top;
        public int Width;
        public int Height;
    }
}
