﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AoC2018.Day14
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Part1();
            Part2();
        }

        private static void Part1()
        {
            var recipes = new LinkedList<short>();
            recipes.AddFirst(3);
            recipes.AddLast(7);
            LinkedListNode<short>[] elves = new[] { recipes.First, recipes.Last };

            while (recipes.Count < 293811)
            //while (recipes.Count < 19)
            {
                int newRecipe = elves.Sum(a => a.Value);
                foreach (short item in newRecipe.ToString().Select(a => short.Parse(new string(new char[] { a }))))
                    recipes.AddLast(item);
                for (int i = 0; i < elves.Length; i++)
                {
                    int moveAmount = elves[i].Value + 1;
                    while (moveAmount-- > 0)
                        elves[i] = elves[i].Next == null ? recipes.First : elves[i].Next;
                }
            }
            string answer = new string(recipes.Reverse().Take(10).Reverse().Select(a => a.ToString().First()).ToArray());
            Console.WriteLine(answer);
            Console.ReadLine();
        }

        private static void Part2()
        {
            var recipes = new List<short>();
            recipes.Add(3);
            recipes.Add(7);
            var elves = new int[] { 0, 1 };

            string digits = "";
            string digits2 = "";
            while (true)
            //while (recipes.Count < 19)
            {
                int newRecipe = elves.Sum(a => recipes[a]);
                recipes.AddRange(newRecipe.ToString().Select(a => short.Parse(new string(new char[] { a }))));

                for (int i = 0; i < elves.Length; i++)
                {
                    int moveAmount = recipes[elves[i]]+ 1;
                    elves[i] += moveAmount;
                    while (elves[i] >= recipes.Count)
                        elves[i] -= recipes.Count;
                }
                if (newRecipe > 9 && recipes.Count > 10)
                    digits = recipes.GetRange(recipes.Count - 7, 6).Aggregate("", (a,b)=>a+=b.ToString());
                if (recipes.Count > 10)
                   digits2 = recipes.GetRange(recipes.Count - 6, 6).Aggregate("", (a,b)=>a+=b.ToString());
                if (digits == "293801" || digits2 == "293801")
                    break;
            }
            //paused here and manually looked to see if it was digits or digits2, and subtrcted 7 if it was digits2 (it was)
            var answer = recipes.Count - 6;
            Console.WriteLine(answer);
            Console.ReadLine();
        }
    }
}
