﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AoC2018.Common;

namespace AoC2018.Day10
{
    class Program
    {
        static void Main(string[] args)
        {
            //Part1();
            Part2();
        }

        private static void Part1()
        {
            var groups = File.ReadAllLines("input.txt").Select(a => Regex.Match(a, @".*n=< *([-\d]+), *([-\d]+).*y=< *([-\d]+), *([-\d]+).*").Groups);
            var lights = groups.Select((a, b) => (i: b, x: a[1].Value.To<int>(), y: a[2].Value.To<int>(), xd: a[3].Value.To<int>(), yd: a[4].Value.To<int>())).ToArray();

            while (lights.Max(a => a.x) - lights.Min(a => a.x) > 80 && lights.Max(a => a.y) - lights.Min(a => a.y) > 50)
                MoveLights(lights);

            do
            {
                OutputLights(lights);
                MoveLights(lights);
            } while (Console.ReadLine() != "x");
        }

        private static int count = 0;
        private static void Part2()
        {
            var groups = File.ReadAllLines("input.txt").Select(a => Regex.Match(a, @".*n=< *([-\d]+), *([-\d]+).*y=< *([-\d]+), *([-\d]+).*").Groups);
            var lights = groups.Select((a, b) => (i: b, x: a[1].Value.To<int>(), y: a[2].Value.To<int>(), xd: a[3].Value.To<int>(), yd: a[4].Value.To<int>())).ToArray();

            while (lights.Max(a => a.x) - lights.Min(a => a.x) > 80 && lights.Max(a => a.y) - lights.Min(a => a.y) > 20)
                MoveLights(lights);

            do
            {
                OutputLights(lights);
                MoveLights(lights);
            } while (Console.ReadLine() != "x");
        }

        private static void OutputLights((int i, int x, int y, int xd, int yd)[] lights)
        {
            var xMin = lights.Min(a => a.x);
            var yMin = lights.Min(a => a.y);
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].x -= xMin;
                lights[i].y -= yMin;
            }
            for (int i = 0; i < 20; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < 80; j++)
                {
                    if (lights.Any(a => a.x == j && a.y == i))
                        Console.Write('#');
                    else
                        Console.Write('.');
                }
            }
            Console.WriteLine();
            Console.WriteLine($"After {count} seconds");
            Console.WriteLine();
        }

        private static void MoveLights((int i, int x, int y, int xd, int yd)[] lights)
        {
            count++;
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].x += lights[i].xd;
                lights[i].y += lights[i].yd;
            }
        }
    }
}
