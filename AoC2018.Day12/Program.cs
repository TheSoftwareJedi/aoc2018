﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using AoC2018.Common;

namespace AoC2018.Day12
{
    class Program
    {
        static void Main(string[] args)
        {
            //Part1();
            Part2();
        }

        private static void Part1()
        {
            var lines = File.ReadAllLines("input.txt");
            var pots = lines.First().Substring(15).ToList();
            //add 45 empty pots to the left and rightso I don't have to worry about adding pots on the fly. 20 generations and a pot can't spread more than two
            pots = Enumerable.Repeat('.', 45).Concat(pots).Concat(Enumerable.Repeat('.', 45)).ToList();

            var potSpread = lines.Skip(2).ToDictionary(a => new string(a.Take(5).ToArray()), a => a.Last());

            for (int i = 0; i < 20; i++)
            {
                var newPots = Enumerable.Repeat('.', pots.Count).ToList();
                for (int j = 0; j < pots.Count - 5; j++)
                {
                    var potLayout = new string(pots.GetRange(j, 5).ToArray());
                    if (potSpread.TryGetValue(potLayout, out var newValue))
                        newPots[j + 2] = newValue;
                }
                pots = newPots;
            }

            var answer = 0;
            for (int i = 0; i < pots.Count; i++)
            {
                if (pots[i] == '#')
                    answer += i - 45;
            }

            Console.WriteLine(answer);
            Console.ReadLine();
        }

        private static void Part2()
        {
            var lines = File.ReadAllLines("input.txt");
            var pots = lines.First().Substring(15).ToList();
            //add 45 empty pots to the left and rightso I don't have to worry about adding pots on the fly. 20 generations and a pot can't spread more than two
            pots = Enumerable.Repeat('.', 5000).Concat(pots).Concat(Enumerable.Repeat('.', 5000)).ToList();

            var potSpread = lines.Skip(2).ToDictionary(a => new string(a.Take(5).ToArray()), a => a.Last());

            for (long i = 0; i < 2500; i++)
            {
                var newPots = Enumerable.Repeat('.', pots.Count).ToList();
                for (int j = 0; j < pots.Count - 5; j++)
                {
                    var potLayout = new string(pots.GetRange(j, 5).ToArray());
                    if (potSpread.TryGetValue(potLayout, out var newValue))
                        newPots[j + 2] = newValue;
                }
                pots = newPots;

                if ((i + 1) % 20 == 0)
                {
                    var answer = 0;
                    for (int ai = 0; ai < pots.Count; ai++)
                    {
                        if (pots[ai] == '#')
                            answer += ai - 5000;
                    }
                    Console.WriteLine($"{i + 1}, {answer}");
                }
            }
            Console.ReadLine();
        }
    }
}
