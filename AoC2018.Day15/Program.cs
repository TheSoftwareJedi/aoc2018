﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using AoC2018.Common;

namespace AoC2018.Day15
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Part1();
            Part2();
        }

        private static void Part1()
        {
            string[] lines = File.ReadAllLines("input.txt");

            (bool elvesAlive, int score) answer = Calculate(lines, 3, 3);

            Console.WriteLine(answer);
            Console.ReadLine();
        }

        private static void Part2()
        {
            string[] lines = File.ReadAllLines("input.txt");

            int e = 3;
            int answer = 0;
            while (true)
            {
                (bool elvesAlive, int score) result = Calculate(lines, 3, e);
                if (result.elvesAlive)
                {
                    answer = result.score;
                    break;
                }
                e++;
            }

            Console.WriteLine(answer);
            Console.ReadLine();
        }

        private static (bool elvesAlive, int score) Calculate(string[] lines, int g, int e)
        {
            var players = new List<Character>();
            var map = new Square[lines.Length, lines[0].Length];
            for (int i = 0; i < lines.Length; i++)
            {
                for (int j = 0; j < lines[0].Length; j++)
                {
                    switch (lines[i][j])
                    {
                        case '#':
                            map[i, j] = new Cave();
                            break;
                        case '.':
                            map[i, j] = new Blank();
                            break;
                        case var ch when ch == 'G' || ch == 'E':
                            var player = new Character
                            {
                                elf = ch == 'E',
                                attack = ch == 'E' ? e : g,
                                pos = (i, j)
                            };
                            players.Add(player);
                            map[i, j] = player;
                            break;
                    }
                }
            }
            var state = new GameState { map = map, chars = players };

            int ticks = 0;
            int elfCount = state.chars.Count(player => player.elf);
            while (Tick(state))
                ticks++;

            return (state.chars.Count(p => p.elf) == elfCount, (ticks - 1) * state.chars.Select(player => player.life).Sum());
        }

        private static bool Tick(GameState state)
        {
            bool moved = false;
            foreach (Character player in state.chars.OrderBy(a => a.pos))
                if (player.life > 0)
                    if (Fight(state, player))
                        moved = true;
                    else
                    {
                        moved |= Move(state, player);
                        moved |= Fight(state, player);
                    }
            return moved;
        }

        private static bool Move(GameState state, Character player)
        {
            IEnumerable<(Character player, (int x, int y) firstStep)> opponents = FindClosest(state, player);
            if (!opponents.Any())
            {
                return false;
            }
            (Character player, (int irow, int icol) firstStep) opponent = opponents.OrderBy(a => a.player.pos).First();
            (int irow, int icol) nextPos = opponents.Where(a => a.player == opponent.player).Select(a => a.firstStep).OrderBy(_ => _).First();
            (state.map[nextPos.irow, nextPos.icol], state.map[player.pos.x, player.pos.y]) =
                (state.map[player.pos.x, player.pos.y], state.map[nextPos.irow, nextPos.icol]);
            player.pos = nextPos;
            return true;
        }

        private static IEnumerable<(Character player, (int x, int y) firstStep)> FindClosest(GameState state, Character player)
        {
            int minDist = int.MaxValue;
            foreach ((Character otherPlayer, (int x, int y) firstStep, int dist) in PathFind(state, player))
            {
                if (dist > minDist)
                {
                    break;
                }
                else
                {
                    minDist = dist;
                    yield return (otherPlayer, firstStep);
                }
            }
        }

        private static bool ValidPosition(GameState state, (int x, int y) pos)
        {
            return !(pos.x < 0 || pos.x >= state.map.GetLength(0) || pos.y < 0 || pos.y >= state.map.GetLength(1));
        }

        private static Square GetSquare(GameState state, (int x, int y) pos)
        {
            return ValidPosition(state, pos) ? state.map[pos.x, pos.y] : new Cave();
        }

        private static IEnumerable<(Character player, (int x, int y) firstStep, int dist)> PathFind(GameState state, Character player)
        {
            var seen = new HashSet<(int x, int y)>
            {
                player.pos
            };
            var q = new Queue<((int x, int y) pos, (int drow, int dcol) origDir, int dist)>();

            foreach ((int drow, int dcol) in new[] { (-1, 0), (0, -1), (0, 1), (1, 0) })
            {
                (int, int) posT = (player.pos.x + drow, player.pos.y + dcol);
                q.Enqueue((posT, posT, 1));
            }

            while (q.Any())
            {
                ((int irow, int icol) pos, (int drow, int dcol) firstStep, int dist) = q.Dequeue();
                switch (GetSquare(state, pos))
                {
                    case Character otherPlayer when player != otherPlayer && otherPlayer.elf != player.elf:
                        yield return (otherPlayer, firstStep, dist);
                        break;

                    case Cave _:
                        break;

                    case Blank _:
                        foreach ((int drow, int dcol) in new[] { (-1, 0), (0, -1), (0, 1), (1, 0) })
                        {
                            (int, int) posT = (pos.x + drow, pos.y + dcol);
                            if (!seen.Contains(posT))
                            {
                                seen.Add(posT);
                                q.Enqueue((posT, firstStep, dist + 1));
                            }
                        }
                        break;
                }
            }
        }

        private static bool Fight(GameState state, Character player)
        {
            var opponents = new List<Character>();

            foreach ((int dx, int dy) in new[] { (-1, 0), (0, -1), (0, 1), (1, 0) })
            {
                (int, int) delta = (player.pos.x + dx, player.pos.y + dy);
                Square block = GetSquare(state, delta);
                switch (block)
                {
                    case Character otherPlayer when otherPlayer.elf != player.elf:
                        opponents.Add(otherPlayer);
                        break;
                }
            }

            if (!opponents.Any())
                return false;
            int minHp = opponents.Select(a => a.life).Min();
            Character o = opponents.First(a => a.life == minHp);
            o.life -= player.attack;
            if (o.life <= 0)
            {
                state.chars.Remove(o);
                state.map[o.pos.x, o.pos.y] = new Blank();
            }
            return true;
        }

    }

    public class GameState
    {
        public Square[,] map;
        public List<Character> chars;
    }

    public abstract class Square
    {
    }

    public class Cave : Square
    {
    }

    public class Character : Square
    {
        public (int x, int y) pos;
        public bool elf;
        public int attack = 3;
        public int life = 200;
    }

    public class Blank : Square
    {
    }
}