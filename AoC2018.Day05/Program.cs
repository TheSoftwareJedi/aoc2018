﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AoC2018.Common;

namespace AoC2018.Day5
{
    class Program
    {
        static int diff = 'a'-'A';

        static void Main(string[] args)
        {
            Part1();
            //Part2();
        }

        private static void Part1() {
            var sw = new Stopwatch();
            var line =File.ReadAllLines("input.txt").First();
            sw.Start();
            string oldLine = "";
            while (oldLine != line)
            {
                oldLine = line;
                //line = line.Aggregate("", (a, b) => a == "" ? b.ToString() : a.Last() == (char)(b ^ ' ') ? a.Substring(0, a.Length - 1) : a + b);
                line = line.Aggregate("0", (a, b) => a.Last() == (char)(b ^ ' ') ? new string(a.Reverse().Skip(1).Reverse().ToArray()) : a + b).Substring(1);
            }
            Console.WriteLine($@"got {line.Count()} in {sw.Elapsed.Milliseconds}ms");
            Console.ReadLine();
        }

        private static void Part2()
        {
            var origLine =File.ReadAllLines("input.txt").First();
            char minC = '0';
            int min = int.MaxValue;
            foreach (char c in Enumerable.Range('a', 26))
            {
                var line = origLine;
                line = line.Replace(new string(c, 1), "");
                line = line.Replace(new string(c, 1).ToUpper(), "");
                string oldLine = "";
                while (oldLine != line)
                {
                    Console.WriteLine(line.Count());
                    oldLine = line;
                    line = line.Aggregate("0", (a, b) => a.Last() == (char)(b ^ ' ') ? new string(a.Reverse().Skip(1).Reverse().ToArray()) : a + b).Substring(1);
                }
                Console.WriteLine($"{c} = {line.Count()}");
                if (line.Count() < min)
                {
                    minC = c;
                    min = line.Count();
                }
            }
            Console.WriteLine($"{minC} = {min}");
            Console.ReadLine();
        }
    }
}
