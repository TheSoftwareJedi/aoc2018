﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using AoC2018.Common;

namespace AoC2018.Day11
{
    class Program
    {
        static void Main(string[] args)
        {
            //Part1(5153);
            Part2(5153);
        }

        private static void Part1(int serial)
        {
            //build the grid
            int[,] powerGrid = new int[300, 300];
            for (int y = 0; y < powerGrid.GetLength(0); y++)
                for (int x = 0; x < powerGrid.GetLength(1); x++)
                {
                    var t = ((x + 10) * y + serial) * (x + 10);
                    t = t < 100 ? 0 : t.ToString().Reverse().Skip(2).Take(1).First().ToString().To<int>();
                    t -= 5;
                    powerGrid[x, y] = t;
                }

            //find the max power
            int maxPower = int.MinValue;
            (int x, int y) maxCoord = (-1, -1);
            try
            {
                for (int y = 0; y < powerGrid.GetLength(0) - 3; y++)
                    for (int x = 0; x < powerGrid.GetLength(1) - 3; x++)
                    {
                        int power = 0;
                        for (int i = 0; i < 3; i++)
                            for (int j = 0; j < 3; j++)
                                power += powerGrid[x + i, y + j];
                        if (power > maxPower)
                        {
                            maxPower = power;
                            maxCoord = (x, y);
                        }
                    }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine();
            Console.WriteLine($"coord {maxCoord} = {maxPower}");
            Console.ReadLine();
        }

        private static void Part2(int serial)
        {
            //build the grid
            int[,] powerGrid = new int[301, 301];
            for (int y = 1; y < powerGrid.GetLength(0); y++)
                for (int x = 1; x < powerGrid.GetLength(1); x++)
                {
                    var t = ((x + 10) * y + serial) * (x + 10);
                    t = t < 100 ? 0 : t.ToString().Reverse().Skip(2).Take(1).First().ToString().To<int>();
                    t -= 5;
                    powerGrid[x, y] = t;
                }

            //find the max power
            int maxPower = int.MinValue;
            (int x, int y, int s) maxCoord = (-1, -1, -1);

            for (int y = 1; y < powerGrid.GetLength(0); y++)
                for (int x = 1; x < powerGrid.GetLength(1); x++)
                    for (int s = 1; s < 301 - (x > y ? x : y); s++)
                    {
                        int power = 0;
                        for (int i = 0; i < s; i++)
                            for (int j = 0; j < s; j++)
                                power += powerGrid[x + i, y + j];
                        if (power > maxPower)
                        {
                            maxPower = power;
                            maxCoord = (x, y, s);
                        }
                    }
            Console.WriteLine($"coord {maxCoord} = {maxPower}");
            Console.ReadLine();
        }
    }
}
